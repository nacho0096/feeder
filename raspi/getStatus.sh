#! /bin/bash
date +%D' PST '%H:%M:%S | awk '{ printf "%s",$0}'
echo -n " Status RaspberryPi:OK "
ls /dev/ttyACM0 >/dev/null 2>&1 #check wheter the arduino is OK
if [[ $? -eq 0 ]]
then
	echo -n "Arduino:OK "
else
	echo -n "Arduino:FAIL "
fi
if [[ -e stuck ]]
then
	echo "Stuck:TRUE "
else
	echo "Stuck:FALSE "
fi
