/*
 * Inaki Gonzalo
 Arduino Program to do feedings on demand.


 */
#define minimumTime 300 //minimum time in between presses
#define maximumTime 1700 //maximum time in between presses
int sensorPin = A0; //push button
int motorPin = A1; 
int motorPin2 = A2; //add extra current

void setup() {
  
  pinMode(sensorPin, INPUT);
  pinMode(motorPin, OUTPUT);
  pinMode(motorPin2, OUTPUT);
 
  // initialize serial communication:
  Serial.begin(9600);
  
}
//this function moves the motor to do the feeding
//returns whther the the feeding succeded (true for success)
boolean feed(int portionsDesired){
  unsigned long last; //time from last tick or the begining of feeding
  int repeatCounter=0;
  int index=0;
  int portionsGiven=0;
  const int ARRAYSIZE=11;
  int array[ARRAYSIZE];
  boolean pressed=false;
   for(int i=0;i<ARRAYSIZE;i++){
    array[i]=1;
  }
   
   analogWrite(motorPin,250);//start motor
   analogWrite(motorPin2,250);//start motor
   delay(150);//give the motor time to reach average speed
   boolean done=false;
   last=millis();//reset timer to current time
   while(!done){
    int sensorValue = analogRead(sensorPin);//sample the push button every 10ms
    //sensorValue=0 means the button is pressed
    array[index]=sensorValue;//save the read value to buffer
    pressed=true;
    for(int i=0;i<ARRAYSIZE;i++){
      if(array[i]!=0){//if any of the values in the buffer is not zero then set pressed to false
        pressed=false;
      }
    }
    long difference=millis()-last; // time in between tics (the motor tics once for every portion) 
     if(difference>maximumTime|| repeatCounter>2){ //arbitrary number I have chosen as too long in between ticks
      analogWrite(motorPin,0);//turn off motor
      analogWrite(motorPin2,0);//turn off motor
       Serial.print("Feeding Failed, ");
       Serial.print(portionsGiven);
       Serial.println(" portions given");
       return false;
     }
    if(pressed &&difference<minimumTime){//the button was pressed too soon 
      pressed=false;//the press is not valid
      for(int i=0;i<ARRAYSIZE;i++){//clear the pressed buffer
        array[i]=1;
      }
      repeatCounter++;//keep track of how many times this happens, we may be stuck
     last=millis();
    }
    if(pressed){//everything went well a valid press
      portionsGiven++;
      for(int i=0;i<ARRAYSIZE;i++){
        array[i]=1;
      }
      last=millis();
      repeatCounter=0;
    }
    
    index++;
    if(index>=ARRAYSIZE){
      index=0;
    }
   
  
      if(portionsGiven>=portionsDesired){
        delay(100);//let it run a little after the last tick
        analogWrite(motorPin,0);
        analogWrite(motorPin2,0);
        Serial.print("Feeding Succeded, ");
        Serial.print(portionsGiven);
        Serial.println(" portions given");
        return true;
      }
     
       delay(10);
      
     }//ends while loop

  
}
void loop() {
   while (!Serial.available() > 0) {
       delay(2000);
    }
    int desiredPortions=Serial.read()-'0';
    if( desiredPortions>0 &&  desiredPortions<10){
      feed(desiredPortions); 
    }
    else{
      Serial.println("Invalid number of portions");
    }

}









